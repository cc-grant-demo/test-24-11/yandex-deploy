locals {
  portdemo = 6432
}

data "yandex_vpc_network" "network-demo" {
  name = "default"
}

data "yandex_vpc_subnet" "subnet-demo" {
  name = "default-ru-central1-a"
}

resource "yandex_vpc_security_group" "sg-demo" {
  name       = "sg-demo"
  network_id = data.yandex_vpc_network.network-demo.id

  ingress {
    protocol       = "TCP"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = local.portdemo
    to_port        = local.portdemo
  }

  egress {
    protocol       = "TCP"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = local.portdemo
    to_port        = local.portdemo
  }

  labels = {
    name       = "demo"
    managed-by = "terraform"
  }
}

resource "yandex_mdb_postgresql_cluster" "cluster-db-demo" {
  name                = "demo"
  environment         = "PRODUCTION"
  network_id          = data.yandex_vpc_network.network-demo.id
  security_group_ids  = [yandex_vpc_security_group.sg-demo.id]
  deletion_protection = false

  config {
    version = "13"
    resources {
      resource_preset_id = "s2.micro"
      disk_type_id       = "network-ssd"
      disk_size          = 15
    }
  }

  database {
    name  = "demo"
    owner = "demouser"
  }

  user {
    name     = "demouser"
    password = "demopass"
    permission {
      database_name = "demo"
    }
  }

  host {
    zone             = local.zone
    subnet_id        = data.yandex_vpc_subnet.subnet-demo.id
    assign_public_ip = true
  }
}

resource "kubernetes_service" "external-service-demo" {
  metadata {
    name      = "service-demo"
    namespace = var.namespace
  }

  spec {
    type = "ExternalName"
    external_name = "c-${yandex_mdb_postgresql_cluster.cluster-db-demo.id}.rw.mdb.yandexcloud.net"
  }
}