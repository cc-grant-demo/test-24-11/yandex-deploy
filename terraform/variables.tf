variable "region" {
  type = string
}

variable "key" {
  type = string
}

variable "namespace" {
  type = string
}

variable "kubeconfig" {
  type = string
}

locals {
  region     = var.region
  zone       = "${local.region}-a"
}